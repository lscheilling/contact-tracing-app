from flask import Flask, render_template, request, redirect, url_for, session
from wtforms import Form, BooleanField, StringField, PasswordField, HiddenField, validators
import sqlalchemy
from sqlalchemy import text
import pymysql
from datetime import datetime
from passlib.hash import sha256_crypt as sha256
from urllib.parse import urlparse
import os

STYLES = ["theme.css"]
JAVASCRIPT = []
YEAR = datetime.now().year

class RegistrationForm(Form):
    username = StringField('Username', [validators.Length(min=4, max=25, message="Username must be between 4 and 25 characters long")])
    email = StringField('Email Address', [validators.Email(message="Email must be valid")])
    password = PasswordField('New Password', [validators.Length(min=10, max=25, message="Password must be between 10 and 25 characters long"),validators.Regexp("(?=.*[a-z])", message="Password must contain a lowercase letter"),validators.Regexp("(?=.*[A-Z])", message="Password must contain a uppercase letter"),validators.Regexp("(?=.*\d)", message="Password must contain a digit")])
    confirm = PasswordField('Repeat Password', [validators.EqualTo('password', message='Passwords must match')])
    accept_tos = BooleanField('I accept the TOS', [validators.DataRequired()])
    register = HiddenField('')

class RegistrationStaffForm(Form):
    username = StringField('Username', [validators.Length(min=4, max=25, message="Username must be between 4 and 25 characters long")])
    password = PasswordField('New Password', [validators.Length(min=10, max=25, message="Password must be between 10 and 25 characters long"),validators.Regexp("(?=.*[a-z])", message="Password must contain a lowercase letter"),validators.Regexp("(?=.*[A-Z])", message="Password must contain a uppercase letter"),validators.Regexp("(?=.*\d)", message="Password must contain a digit")])
    confirm = PasswordField('Repeat Password', [validators.EqualTo('password', message='Passwords must match')])
    register = HiddenField('')

class VenueContactDetailsForm(Form):
    id = StringField('Venue ID', [validators.DataRequired(),validators.Length(min=5, max=30, message="Venue ID must be between 5 and 30 characters long")])
    venue = HiddenField('')

class LoginForm(Form):
    username = StringField('Username', [validators.DataRequired()])
    password = PasswordField('Password', [validators.DataRequired()])
    login = HiddenField('')

class VenueContactForm(Form):
    phone = StringField('Phone Number', [validators.DataRequired(),validators.Length(min=7, max=20, message="Number must be of valid length"),validators.Regexp('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$',message="Number must be of valid format")])
    email = StringField('Email Address', [validators.Optional(),validators.Email(message="Email must be valid")])
    venue = StringField('Venue ID', [validators.DataRequired()])
    venueContact = HiddenField('')

class CreateVenueForm(Form):
    name = StringField('Venue Name', [validators.DataRequired(),validators.Length(min=4, max=25, message="Venue Name must be between 4 and 25 characters long")])
    venue = HiddenField('')

def setup_app():
    verify_database() #Make sure database tables exise
    app = Flask(__name__, template_folder='root', static_folder = 'static') #Create server for webpage
    app.config.from_object(__name__)
    app.secret_key = "x*G]uX2Rn$}5M_Y5"
    return app

def verify_database():
    with db.connect() as c:
        c.execute(text("CREATE TABLE IF NOT EXISTS client_account (username VARCHAR(64) NOT NULL, password VARCHAR(256) NOT NULL, email VARCHAR(128) NOT NULL UNIQUE, tier TINYINT DEFAULT 0, PRIMARY KEY (username))"))
        c.execute(text("CREATE TABLE IF NOT EXISTS staff_account  (username VARCHAR(64) NOT NULL, password VARCHAR(256) NOT NULL, tier TINYINT DEFAULT 0, PRIMARY KEY (username))"))
        c.execute(text("CREATE TABLE IF NOT EXISTS venue_details  (id VARCHAR(32) NOT NULL, owner VARCHAR(64) NOT NULL, name VARCHAR(32) NOT NULL, active TINYINT DEFAULT 1, PRIMARY KEY (id), FOREIGN KEY (owner) REFERENCES client_account(username))"))
        c.execute(text("CREATE TABLE IF NOT EXISTS contact_details (phone VARCHAR(32) NOT NULL, email VARCHAR(128), venue VARCHAR(32) NOT NULL, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY (venue) REFERENCES venue_details(id))"))
        

def redirect_to_prev(alternate="homepage",url=False):
    if not url:
        url = request.referrer
    if url and urlparse(url).netloc == request.host:
        return redirect(url)
    return redirect(url_for(alternate))

#functions to handle POST requests

def process_login(form):
    rows = None
    with db.connect() as c:
        res = c.execute(text("SELECT username, password, email, tier FROM client_account WHERE username=:user LIMIT 1"),user=str.lower(form.username.data))
        rows = res.fetchall()
        
    if len(rows) == 0:
        return process_loginStaff(form) #no clients exist so check if staff exists
    else:
        q = rows[0]
        if sha256.verify(form.password.data,q[1]): #login successfull
            session["logged_in"] = True
            session["account_type"] = 0
            session["username"] = q[0]
            session["email"] = q[2]
            session["tier"] = q[3]
            return redirect_to_prev("profile",session["prev_url"])
        else:
            return process_loginStaff(form) #password is wrong so instead try logging into staff acount

def process_loginStaff(form):
    rows = None
    with db.connect() as c:
        res = c.execute(text("SELECT username, password, tier FROM staff_account WHERE username=:user LIMIT 1"),user=str.lower(form.username.data))
        rows = res.fetchall()
        
    if len(rows) == 0:
        form.password.errors.append('Username or Password is incorrect') #no staff exists for username so give the user an error
        return render_template('/login.html', path="login", loginform=form, registerform=RegistrationForm(), css=STYLES, js=JAVASCRIPT, year=YEAR)
    else:
        q = rows[0]
        if sha256.verify(form.password.data,q[1]): #login successfull
            session["logged_in"] = True
            session["account_type"] = 1
            session["username"] = q[0]
            session["tier"] = q[2]
            return redirect_to_prev("profile",session["prev_url"])
        else:
            form.password.errors.append('Username or Password is incorrect') #wrong password so give user an error
            return render_template('/login.html', path="login", loginform=form, registerform=RegistrationForm(), css=STYLES, js=JAVASCRIPT, year=YEAR) 



def process_register(form):
    with db.connect() as c:
        res = c.execute(text("SELECT username, email FROM client_account WHERE username=:user OR email=:email"),user=str.lower(form.username.data),email=form.email.data)
        rows = res.fetchall()
        if len(rows) > 0: #an account exists with the username or email so give user an error
            for row in rows:
                if row[0] == form.username.data:
                    form.username.errors.append('Username already exists')
                if row[1] == form.email.data:
                    form.email.errors.append('Email already exists')
            
            return render_template('/login.html', path="register", loginform=LoginForm(), registerform=form, css=STYLES, js=JAVASCRIPT, year=YEAR)
        else: #registration successfull
            c.execute(text("INSERT INTO client_account (username,password,email) VALUES (:user,:password,:email)"),user=str.lower(form.username.data),password=sha256.hash(form.password.data),email=form.email.data)
            
            session["logged_in"] = True #login to newly created account
            session["account_type"] = 0
            session["username"] = form.username.data
            session["email"] = form.email.data
            session["tier"] = 0
            return redirect_to_prev("profile",session["prev_url"])

def process_registerStaff(form):
    with db.connect() as c:
        res = c.execute(text("SELECT username FROM staff_account WHERE username=:user"),user=str.lower(form.username.data))
        rows = res.fetchall()
        if len(rows) > 0: #an account exists with the username or email so give user an error
            form.username.errors.append('Username already exists')
            
            return render_template('/staff_profile.html', profile=get_profile(session["username"]), venueForm=VenueContactDetailsForm(), path="new-staff", staffForm=form, css=STYLES, js=JAVASCRIPT, year=YEAR)
        else: #registration successfull
            c.execute(text("INSERT INTO staff_account (username,password) VALUES (:user,:password)"),user=str.lower(form.username.data),password=sha256.hash(form.password.data))
            
            return render_template('/staff_profile.html', profile=get_profile(session["username"]), venueForm=VenueContactDetailsForm(), path="new-staff", staffForm=RegistrationStaffForm(), css=STYLES, js=JAVASCRIPT, year=YEAR)

def process_VenueContactDetails(form):
    with db.connect() as c:
        res = c.execute(text("SELECT phone, email FROM contact_details WHERE venue=:id"),id=str.lower(form.id.data))
        rows = res.fetchall()
        if len(rows) > 0: #success
            
            
            return render_template('/staff_profile.html', profile=get_profile(session["username"]), results=rows, path="venue-contact", staffForm=RegistrationStaffForm(), css=STYLES, js=JAVASCRIPT, year=YEAR)
        else: #fail
            form.id.errors.append('No Results')
            
            
            return render_template('/staff_profile.html', profile=get_profile(session["username"]), venueForm=form, path="venue-contact", staffForm=RegistrationStaffForm(), css=STYLES, js=JAVASCRIPT, year=YEAR)

def process_venueContact(form):
    with db.connect() as c:
        try:
            res = c.execute(text("SELECT client_account.tier FROM venue_details, client_account WHERE venue_details.id = :id AND venue_details.active = 1 AND venue_details.owner = client_account.username"),id=str.lower(form.venue.data))
            results = res.fetchall()
            if len(results) == 1:
                tier = results[0][0]
                
                res = c.execute(text("SELECT phone FROM contact_details WHERE venue=:id"),{id:str.lower(form.venue.data)})
                phones = res.fetchall()
                
                if len(list(set([x for x in phones if phones.count(x) >= 1]))) < {0:5000,1:100000,2:500000}.get(tier):
                    c.execute(text("INSERT INTO contact_details(phone, email, venue) VALUES (:phone,:email,:id)"),phone=form.phone.data,email=form.email.data,id=str.lower(form.venue.data))
                    
                    return render_template('/venue.html', form=form, success=True, css=STYLES, js=JAVASCRIPT, year=YEAR)
                else:
                    form.venue.errors.append('Venue is too popular, please try again another time...')
                    return render_template('/venue.html', form=form, success=False, css=STYLES, js=JAVASCRIPT, year=YEAR)
            else:
                form.venue.errors.append('Venue ID is invalid')
                return render_template('/venue.html', form=form, success=False, css=STYLES, js=JAVASCRIPT, year=YEAR)
        except sqlite3.IntegrityError:
            form.venue.errors.append('Venue ID is invalid')
            return render_template('/venue.html', form=form, success=False, css=STYLES, js=JAVASCRIPT, year=YEAR)

def process_createVenue(form):
    profile = get_profile(session["username"])
    if profile["allowedCount"] > profile["venueCount"]:
        with db.connect() as c:
            id = form.name.data
            for i in range(1000):
                try:
                    c.execute(text("INSERT INTO venue_details(id, name, owner) VALUES (:id,:name,:owner)"),id=str.lower(form.name.data + str(i)).replace(" ","_"),name=form.name.data,owner=session["username"])
                    
                    id=str.lower(form.name.data + str(i)).replace(" ","_")
                    return render_template('/client_profile.html', venueForm=CreateVenueForm(), path="venue-"+id, profile=get_profile(session["username"]), success=True, css=STYLES, js=JAVASCRIPT, year=YEAR)
                except SQLAlchemyError:
                    continue
            form.name.errors.append('Venue Name Taken')
            return render_template('/client_profile.html', venueForm=form, path="create-venue", profile=profile, success=True, css=STYLES, js=JAVASCRIPT, year=YEAR)
    form.name.errors.append('No more venues allowed!')
    return render_template('/client_profile.html', venueForm=form, path="create-venue", profile=profile, success=True, css=STYLES, js=JAVASCRIPT, year=YEAR)

def process_upgrade(username=False):
    if not username:
        username = session["username"]
    print("UPGRADE "+username)
    with db.connect() as c:
        if session["account_type"] == 0:
            c.execute(text("UPDATE client_account SET tier = tier + 1 WHERE tier < 2 AND username = :user LIMIT 1"),user=username)
            if session["tier"] < 2:
                session["tier"] += 1
        elif session["account_type"] == 1:
            c.execute(text("UPDATE staff_account SET tier = tier + 1 WHERE tier < 1 AND username = :user LIMIT 1"),user=username)
            if session["tier"] < 1:
                session["tier"] += 1
        

def process_downgrade(username=False):
    if not username:
        username = session["username"]
    print("DOWNGRADE "+username)
    with db.connect() as c:
        if session["account_type"] == 0:
            c.execute(text("UPDATE client_account SET tier = tier - 1 WHERE tier > 0 AND username = :user LIMIT 1"),user=username)
        elif session["account_type"] == 1:
            c.execute(text("UPDATE staff_account SET tier = tier - 1 WHERE tier > 0 AND username = :user LIMIT 1"),user=username)
        if session["tier"] > 0:
            session["tier"] -= 1
        

def process_deleteVenue(id):
    print("DELETE "+id)
    with db.connect() as c:
        if session["account_type"] == 0:
            c.execute(text("UPDATE venue_details SET active = 0 WHERE owner = :owner AND id = :id LIMIT 1"),owner=session["username"],id=id)
        elif session["account_type"] == 1:
            c.execute(text("UPDATE venue_details SET active = 0 WHERE id = :id LIMIT 1"),id=id)
        

def process_renameVenue(id, name):
    print("RENAME "+id)
    with db.connect() as c:
        if session["account_type"] == 0:
            c.execute(text("UPDATE venue_details SET name = :name WHERE owner = :owner AND id = :id LIMIT 1"),name=name,owner=session["username"],id=id)
        elif session["account_type"] == 1:
            c.execute(text("UPDATE venue_details SET name = :name WHERE id = :id LIMIT 1"),name=name,id=id)
        

def get_profile(username):
    with db.connect() as c:
        profile={"tier":session["tier"],"username":username,"email":session["email"]}
        if session["account_type"] == 0:
            res = c.execute(text("SELECT id, name FROM venue_details WHERE owner=:owner AND active=1"),owner=username)
            venues = res.fetchall()
            
            profile["venues"] = []
            profile["userAvg"] = 0
            for i in range(len(venues)):
                res = c.execute(text("SELECT phone FROM contact_details WHERE venue=:id"),id=venues[i][0])
                phones = res.fetchall()
                
                profile["venues"].append((venues[i][0],venues[i][1],len(list(set([x for x in phones if phones.count(x) >= 1]))),len(list(set([x for x in phones if phones.count(x) >= 2]))),len(list(set([x for x in phones if phones.count(x) >= 10]))),))
                profile["userAvg"] += profile["venues"][-1][2]
            if len(profile["venues"]) > 0:
                profile["userAvg"] = round(profile["userAvg"]/len(profile["venues"]))
            else:
                profile["userAvg"] = 0
            profile["venueCount"] = len(profile["venues"])
            profile["allowedCount"] = {0:1,1:10,2:1000}.get(profile["tier"])
            profile["tierName"] = {0:"Free",1:"Premium",2:"Ultimate"}.get(profile["tier"])
            profile["userCap"] = {0:"5000",1:"100000",2:"500000"}.get(profile["tier"])
        else:
            profile["tierName"] = {0:"Staff",1:"Administrator"}.get(profile["tier"])
        return profile

db = sqlalchemy.create_engine(os.environ.get('DB_STRING'))
app = setup_app()

@app.route('/')
def homepage():
    return render_template('/index.html', css=STYLES, js=JAVASCRIPT, year=YEAR)

@app.route('/about')
def about():
    return render_template('/about.html', css=STYLES, js=JAVASCRIPT, year=YEAR)

@app.route('/pricing')
def pricing():
    return render_template('/pricing.html', css=STYLES, js=JAVASCRIPT, year=YEAR)

@app.route('/faq')
def faq():
    return render_template('/faq.html', css=STYLES, js=JAVASCRIPT, year=YEAR)

@app.route('/terms-of-service')
def tos():
    return render_template('/tos.html', css=STYLES, js=JAVASCRIPT, year=YEAR)

@app.route('/privacy-policy')
def privacy():
    return render_template('/privacy.html', css=STYLES, js=JAVASCRIPT, year=YEAR)

@app.route('/logout')
def logout():
    session["logged_in"] = False
    return redirect_to_prev()

@app.route('/display')
def qrcode():
    return render_template('/code.html', host=request.host_url)

# DYNAMIC PAGES

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.referrer and urlparse(request.referrer).path != request.path:
        session["prev_url"] = request.referrer
    elif not "prev_url" in session.keys():
        session["prev_url"] = False
    if "logged_in" in session and session["logged_in"]:
        if session["account_type"] == 0:
            return redirect(url_for("profile"))
        elif session["account_type"] == 1:
            return redirect(url_for("profile"))
    req = request.form
    lform = LoginForm(req)
    rform = RegistrationForm(req)
    if request.method == 'POST':
        if 'login' in req and lform.validate():
            return process_login(lform)
        elif 'register' in req:
            if rform.validate():
                return process_register(rform)
            return render_template('/login.html', loginform=LoginForm(), registerform=rform, path="register", css=STYLES, js=JAVASCRIPT, year=YEAR)
    return render_template('/login.html', loginform=lform, path="login", registerform=rform, css=STYLES, js=JAVASCRIPT, year=YEAR)

@app.route('/venue', methods=['GET', 'POST'])
def venue():
    req = request.form
    form = VenueContactForm(req)
    if request.method == 'POST' and 'venueContact' in req and form.validate():
        return process_venueContact(form)
    elif request.method == 'GET' and 'id' in request.args and request.args["id"] != "":
        form.venue.data = request.args["id"]
    return render_template('/venue.html', form=form, success=False, css=STYLES, js=JAVASCRIPT, year=YEAR)

@app.route('/profile', methods=['GET', 'POST'])
def profile():
    req = request.form
    if not("logged_in" in session and session["logged_in"]):
        return redirect(url_for("homepage"))
    if session["account_type"] == 0:
        venueForm = CreateVenueForm(req)
        if (request.method == 'POST' and "venue" in req and venueForm.validate()):
            return process_createVenue(venueForm)
        elif (request.method == 'POST' and "upgrade-sub" in req):
            process_upgrade()
            return render_template('/client_profile.html', path="subscription", venueForm=venueForm, profile=get_profile(session["username"]), css=STYLES, js=JAVASCRIPT, year=YEAR)
        elif (request.method == 'POST' and "downgrade-sub" in req):
            process_downgrade()
            return render_template('/client_profile.html', path="subscription", venueForm=venueForm, profile=get_profile(session["username"]), css=STYLES, js=JAVASCRIPT, year=YEAR)
        elif (request.method == 'POST' and "delete-ven" in req):
            process_deleteVenue(req["id"])
            return render_template('/client_profile.html', path="details", venueForm=venueForm, profile=get_profile(session["username"]), css=STYLES, js=JAVASCRIPT, year=YEAR)
        elif (request.method == 'POST' and "modify-ven" in req):
            process_renameVenue(req["id"],req["name"])
            return render_template('/client_profile.html', path="venue-"+req["id"], venueForm=venueForm, profile=get_profile(session["username"]), css=STYLES, js=JAVASCRIPT, year=YEAR)
        return render_template('/client_profile.html', venueForm=venueForm, profile=get_profile(session["username"]), css=STYLES, js=JAVASCRIPT, year=YEAR)
    else:
        req = request.form
        venform = VenueContactDetailsForm(req)
        stfform = RegistrationStaffForm(req)
        if request.method == 'POST':
            if 'venue' in req and venform.validate():
                return process_VenueContactDetails(venform)
            elif 'register' in req and stfform.validate():
                return process_registerStaff(stfform)
        return render_template('/staff_profile.html', profile=get_profile(session["username"]), venueForm=venform, staffForm=stfform, css=STYLES, js=JAVASCRIPT, year=YEAR)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=int(os.environ.get('PORT', 8080)))
